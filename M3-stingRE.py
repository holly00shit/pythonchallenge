#! /usr/bin/env python
#coding=utf-8

import re
import string


"""
re.match的函数原型为：re.match(pattern, string, flags)
第一个参数是正则表达式，这里为"(\w+)\s"，如果匹配成功，则返回一个Match，否则返回一个None；
第二个参数表示要匹配的字符串；
第三个参数是标致位，用于控制正则表达式的匹配方式，如：是否区分大小写，多行匹配等等。

match：
"""

fileM3 = open('equality.txt','r')
s = []

for eachline in fileM3.readlines():
    eachline = eachline.strip()
#    print eachline,
    resulte = re.findall("[a-z][A-Z]{3}[a-z][A-Z]{3}[a-z]",eachline)   
    if resulte:
        s.append(resulte)
print s


"""
去掉换行符

fileM3 = open('equality.txt','r')
relist = []
s = ''

for eachline in fileM3.readlines():
    eachline = eachline.strip()
#    print eachline,
    s += eachline
    
print s[:300]
resulte = re.findall("[a-z][A-Z]{3}[a-z][A-Z]{3}[a-z]",s)
print resulte

"""