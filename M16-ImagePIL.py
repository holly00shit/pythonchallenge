#! /usr/bin/env python
#coding=utf-8

"""
http://www.pythonchallenge.com/pc/return/mozart.html
"""
import Image

im = Image.open('mozart.gif')

print im.size

print im.mode

print im.getpixel((630,2))

def straighten(line): #找到第一个粉红色像素，以它为开头，左对齐
    i = 0
    while line[i] != 195:
        i += 1
    return line[i:] + line[:i]

for h  in range(im.size[1]):
    line = [im.getpixel((w,h))for w in range(im.size[0])]
    line = straighten(line)
    [im.putpixel((w,h),line[w]) for w in range(im.size[0])]
    
im.save('16.gif')
#im,show()