#! /usr/bin/env python
#coding=utf-8

import pickle
import urllib

if __name__ == '__main__':
    url = 'http://www.pythonchallenge.com/pc/def/banner.p'
#    request.set_proxy('hzproxy.asiainfo-linkage.com:8080','http')
    
    try:
        data = urllib.urlopen(url).read()
    except Exception as ex:
        print ex 
        
 #   print data
    
    banner = pickle.loads(data)
    print banner
    for line in banner:
        print(''.join(map(lambda x:x[0]*x[1],line))) 
        """
        不是很理解 line[0]是怎么解析的
        其实for x in line:还是以每个最小单位的()去取，而不是每个[].
        可以用下面的代替
        可见lambda隐式函数有多方便
        
        def PX(a):
            a = []
            for x in line:
                a.append(x[0]*x[1])
            print a
        """
                            