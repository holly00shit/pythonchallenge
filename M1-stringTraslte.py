#! /usr/bin/env python
#coding=utf-8

import random
import string
f='''g fmnc wms bgblr rpylqjyrc gr zw fylb. rfyrq ufyr amknsrcpq ypc dmp. bmgle gr gl zw fylb gq glcddgagclr ylb rfyr'q ufw rfgq rcvr gq qm jmle.sqgle qrpgle.kyicrpylq() gq pcamkkclbcb.lmu ynnjw ml rfc spj.'''
#f = '''map.html'''

target = 'abcdefghijklmnopqrstuvwxyz'
TRtarget = 'cdefghijklmnopqrstuvwxyzab'
"""
最佳方法
"""
ftable = string.maketrans(target,TRtarget)
Dtable = string.maketrans('','')
Testrel = f.translate(Dtable,'g')  #删除'g'
result = f.translate(ftable)
print Testrel 

"""
改良的方法

f = f.replace(" ","/")
#f = f.replace('.','|')
flist = f.split('/')
dicYZ = {'y':'a','z':'b'}

q = ''
def changechar(object):
    charl = object
    if charl in ['y','z']:
        charl = dicYZ[charl]
    elif charl in target:
        charl = chr(ord(charl)+2)
    else:
        pass
    return charl
    

print flist
for i  in range(len(flist)):
    j = flist[i]
    if len(j) == 1:
        j = changechar(j)
    else:
        for u in j:
            chj = changechar(u)
            q = q + chj
#            print q
        j = q  
        q = ''
    flist[i] = j
for k in flist:
    print k,
"""


"""
最原始的方法

target = 'abcdefghijklmnopqrstuvwxyz'
PTXT = ('-').join(f.split(' '))
p = 0
Plist = list(PTXT.strip())
#for j in [" ","'","(",")","\n"]:
#    Plist.remove(j)
#print Plist
for i  in Plist:
    if i not in target and i != '-' and i != '.':
        Plist.remove(i)
        print i*10 +'----' 
        print Plist
    elif i == '.':
        Plist[p] = '\n'
        p += 1
    else:
        Plist[p] = chr(ord(Plist[p])+2)
        p += 1
        
print '==========='
for i  in Plist:
     print i,
"""